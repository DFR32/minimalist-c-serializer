#include "serializer.hpp"
#include <cstdio>

#pragma pack(push, 1)
struct dummy_struct
{
	uint8_t a;
	uint32_t b;
	uint64_t c;
	double h;
	uint32_t f;

	dummy_struct()
		:
		a(3),
		b(2),
		c(4),
		h(5.0),
		f(1)
	{
	}

	std::vector<uint8_t> serialize()
	{
		std::vector<uint8_t> result(sizeof(dummy_struct), 0);

		uint8_t i = 0;
		uint8_t* this_ptr = (uint8_t*)this;

		for (size_t i = 0; i < sizeof(dummy_struct); ++i)
		{
			result.at(i) = *(uint8_t*)(this_ptr + i);
		}

		return result;
	}
};
#pragma pack(pop, 1)

#pragma pack(push, 1)
struct dumb_struct
{
	uint8_t a;
	uint32_t b;
	uint64_t c;
	double h;
	uint32_t f;

	dumb_struct()
		:
		a(3),
		b(2),
		c(4),
		h(5.0),
		f(1)
	{
	}
};
#pragma pack(pop, 1)

int main()
{
	dumb_struct a;
	ISerializer serialized_object(a);

	for (uint8_t i = 0; i < serialized_object.type.size(); ++i)
	{
		printf("%02X ", serialized_object.type[i]);
	}
	return 0;
}